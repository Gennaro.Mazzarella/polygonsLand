package com.springboot.polygonsland;

import com.springboot.polygonsland.polygons.Polygon;
import com.springboot.polygonsland.polygons.PolygonsMerger;
import com.springboot.polygonsland.polygons.Square;

public class PhaseOneManager extends PhaseManager {

    @Override
    public void init(int n, double qmin, double qmax) {
        polygonsMerger = new PolygonsMerger(n);

        for (; n > 0; n--) {
            Square square = new Square(qmin + (qmax - qmin) * random.nextDouble(), getRandomName());
            polygonsMerger.addPolygon(square);

            System.out.println("Nuovo " + square.getType() + " creato.\n- Lato: " + square.getWidth() + ".\n");
        }
    }

    @Override
    public void resolvePhase(int days) {
        int day = 1;

        while (day <= days && polygonsMerger.getPoligonsNumber() > 1) {
            Polygon rectangle = polygonsMerger.mergePolygons(getRandomName());

            System.out.println("Giorno " + day + "\nNuovo RETTANGOLO creato.\n- Larghezza: " + rectangle.getWidth()
                    + ".\n- Altezza: " + rectangle.getHeight()
                    + ".\n- Area: " + rectangle.getArea() + ".\n"
            );

            day++;
        }
    }
}
