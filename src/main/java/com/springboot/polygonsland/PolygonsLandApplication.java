package com.springboot.polygonsland;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolygonsLandApplication {
	public static void main(String[] args) {
		SpringApplication.run(PolygonsLandApplication.class, args);
	}
}
