package com.springboot.polygonsland.polygons;

import java.util.ArrayList;
import java.util.Random;

public class PolygonsMerger {
    protected ArrayList<Polygon> polygons;

    protected Random random = new Random();

    public PolygonsMerger() {
        polygons = new ArrayList<>();
    }

    public PolygonsMerger(int poligonsNumber) {
        polygons = new ArrayList<>(poligonsNumber);
    }

    public void addPolygon(Polygon polygon) {
        if(polygons == null) {
            polygons = new ArrayList<>();
        }

        polygons.add(polygon);
    }

    public int getPoligonsNumber() {
        if(polygons == null) {
            return 0;
        }

        return polygons.size();
    }

    public Polygon mergePolygons(String name) {
        Polygon p1 = polygons.get(random.nextInt(polygons.size()));

        return p1.merge(polygons.get(random.nextInt(polygons.size())), name);
    }
}
