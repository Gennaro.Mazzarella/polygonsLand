package com.springboot.polygonsland.polygons;


public abstract class Polygon {
    protected double height;
    protected double width;
    protected String name;

    public Polygon(double height, double width, String name) {
        this.height = height;
        this.width = width;
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public String getName() {
        return "sig. " + getType() + "_" + name;
    }

    public abstract String getType();
    public abstract double getArea();
    public abstract Polygon merge(Polygon polygon, String name);
}
