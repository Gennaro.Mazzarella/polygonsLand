package com.springboot.polygonsland.polygons;

import java.util.Random;

public class Rectangle extends Polygon {
    public Rectangle(double height, double width, String name) {
        super(height, width, name);
    }

    @Override
    public String getType() {
        return "RETTANGOLO";
    }

    @Override
    public double getArea() {
        return height * width;
    }

    @Override
    public Polygon merge(Polygon polygon, String name) {
        Random random = new Random();
        double h;
        double w;

        if (random.nextInt(2) == 0) {
            w = width + polygon.width;
            h = Math.min(height, polygon.height);

        } else {
            w = Math.min(width, polygon.width);
            h = height + polygon.height;
        }

        if (polygon instanceof Square || (polygon instanceof Rectangle && h != w)) {
            return new Rectangle(h, w, name);

        } else if (polygon instanceof Rectangle) {
            return new Square(h, name);
        }

        return null;
    }
}
