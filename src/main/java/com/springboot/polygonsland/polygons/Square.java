package com.springboot.polygonsland.polygons;

import java.util.Random;

public class Square extends Polygon {
    public Square(double side, String name) {
        super(side, side, name);
    }

    @Override
    public String getType() {
        return "QUADRATO";
    }

    @Override
    public double getArea() {
        return 2 * height;
    }

    @Override
    public Polygon merge(Polygon polygon, String name) {
        Random random = new Random();
        double h = height + polygon.height;
        double w = Math.min(height, polygon.height);

        if(random.nextInt(2) == 0) {
            double temp = h;
            h = w;
            w = temp;
        }

        if(polygon instanceof Square) {
                return new Rectangle(h, w, name);

        } else if(polygon instanceof Rectangle) {
            return new Triangle(h, w, name);
        }

        return null;
    }
}
