package com.springboot.polygonsland.rest.polygonsDim;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidDimRangeException extends RuntimeException {
    public InvalidDimRangeException() {
        super("Il range specificato per le dimensioni dei poligoni non è valido");
    }
}
