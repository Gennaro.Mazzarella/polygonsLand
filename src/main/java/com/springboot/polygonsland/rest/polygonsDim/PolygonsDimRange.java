package com.springboot.polygonsland.rest.polygonsDim;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PolygonsDimRange {
    @NotNull
    @Min(0)
    private Double qMin;

    @NotNull
    @Min(0)
    private Double qMax;

    public Double getqMin() {
        return qMin;
    }

    public void setqMin(Double qMin) {
        this.qMin = qMin;
    }

    public Double getqMax() {
        return qMax;
    }

    public void setqMax(Double qMax) {
        this.qMax = qMax;
    }
}
