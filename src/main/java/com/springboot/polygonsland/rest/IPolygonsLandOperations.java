package com.springboot.polygonsland.rest;


public interface IPolygonsLandOperations {
    void resolvePhase(PhaseModel phaseModel);
}
