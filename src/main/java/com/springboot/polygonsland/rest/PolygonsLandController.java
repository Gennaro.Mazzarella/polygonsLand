package com.springboot.polygonsland.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class PolygonsLandController {
    @Autowired
    private IPolygonsLandOperations operations;

    @RequestMapping(value = "/resolvePhase",
            method = RequestMethod.POST,
            consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity resolvePhase(@Valid @RequestBody PhaseModel phaseModel) {
        operations.resolvePhase(phaseModel);

        return new ResponseEntity(HttpStatus.OK);
    }
}