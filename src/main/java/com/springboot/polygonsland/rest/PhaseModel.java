package com.springboot.polygonsland.rest;

import com.springboot.polygonsland.rest.childrenperday.ChildrenPerDay;
import com.springboot.polygonsland.rest.polygonsDim.PolygonsDimRange;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PhaseModel {
    @NotNull
    @Min(1)
    private Integer poligonsNumber;

    @NonNull
    @Min(1)
    @Max(3)
    private Integer phase;

    @NonNull
    @Min(1)
    private Integer days;

    @NonNull
    private PolygonsDimRange polygonsDim;

    private ChildrenPerDay childrenPerDay;

    public Integer getPoligonsNumber() {
        return poligonsNumber;
    }

    public void setPoligonsNumber(Integer poligonsNumber) {
        this.poligonsNumber = poligonsNumber;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public PolygonsDimRange getPolygonsDim() {
        return polygonsDim;
    }

    public void setPolygonsDim(PolygonsDimRange polygonsDim) {
        this.polygonsDim = polygonsDim;
    }

    public ChildrenPerDay getChildrenPerDay() {
        return childrenPerDay;
    }

    public void setChildrenPerDay(ChildrenPerDay childrenPerDay) {
        this.childrenPerDay = childrenPerDay;
    }
}
