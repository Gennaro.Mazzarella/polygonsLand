package com.springboot.polygonsland.rest.childrenperday;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidChildrenPerDayRangeException extends RuntimeException {
    public InvalidChildrenPerDayRangeException() {
        super("Il range specificato per il numero di figli al giorno non è valido");
    }
}
