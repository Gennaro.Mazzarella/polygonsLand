package com.springboot.polygonsland.rest.childrenperday;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MissingChildrenPerDayValueException extends RuntimeException {
    public MissingChildrenPerDayValueException() {
        super("Non è stato specificato range per il numero di figli al giorno");
    }
}
