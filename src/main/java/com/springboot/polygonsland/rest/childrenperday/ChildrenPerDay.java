package com.springboot.polygonsland.rest.childrenperday;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ChildrenPerDay {
    @NotNull
    @Min(1)
    private Integer min;

    @NotNull
    @Min(1)
    private Integer max;

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }
}
