package com.springboot.polygonsland;

import com.springboot.polygonsland.rest.IPolygonsLandOperations;
import com.springboot.polygonsland.rest.PhaseModel;
import com.springboot.polygonsland.rest.childrenperday.ChildrenPerDay;
import com.springboot.polygonsland.rest.childrenperday.InvalidChildrenPerDayRangeException;
import com.springboot.polygonsland.rest.childrenperday.MissingChildrenPerDayValueException;
import com.springboot.polygonsland.rest.polygonsDim.InvalidDimRangeException;
import com.springboot.polygonsland.rest.polygonsDim.PolygonsDimRange;
import org.springframework.stereotype.Component;

@Component
public class PolygonsLandOperations implements IPolygonsLandOperations {
    PhaseManager phaseManager;

    @Override
    public void resolvePhase(PhaseModel phaseModel) {
        PolygonsDimRange polygonsDim = phaseModel.getPolygonsDim();

        if(polygonsDim.getqMin() > polygonsDim.getqMax()) {
            throw new InvalidDimRangeException();
        }

        int phase = phaseModel.getPhase();
        ChildrenPerDay childrenPerDay = phaseModel.getChildrenPerDay();
        switch (phase) {
            case 1:
                phaseManager = new PhaseOneManager();
                checkChildrenPerDay(childrenPerDay, phase);
                break;

            case 2:
                phaseManager = new PhaseTwoManager();
                checkChildrenPerDay(childrenPerDay, phase);
                ((PhaseTwoManager)phaseManager).setChildrenPerDay(childrenPerDay.getMin(), childrenPerDay.getMax());
                break;

            case 3:
                phaseManager = new PhaseThreeManager();
                checkChildrenPerDay(childrenPerDay, phase);
                ((PhaseThreeManager)phaseManager).setChildrenPerDay(childrenPerDay.getMin(), childrenPerDay.getMax());
                break;
        }

        phaseManager.init(phaseModel.getPoligonsNumber(), polygonsDim.getqMin(), polygonsDim.getqMax());

        phaseManager.resolvePhase(phaseModel.getDays());
    }

    private void checkChildrenPerDay(ChildrenPerDay childrenPerDay, int phase) {
        if(phase == 1) {
            if(childrenPerDay != null) {
                System.out.println("La fase 1 prevede un solo figlio per giorno.\nIl valore settato in input sarà ignorato.\n");
            }
        } else {
            if(childrenPerDay == null) {
                throw new MissingChildrenPerDayValueException();
            }

            if(childrenPerDay.getMin() > childrenPerDay.getMax()) {
                throw new InvalidChildrenPerDayRangeException();
            }
        }
    }
}
