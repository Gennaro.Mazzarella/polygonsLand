package com.springboot.polygonsland;

import com.springboot.polygonsland.polygons.PolygonsMerger;

import java.util.ArrayList;
import java.util.Random;

public abstract class PhaseManager {
    protected PolygonsMerger polygonsMerger;

    protected ArrayList<String> names;
    protected Random random = new Random();

    public abstract void init(int n, double qmin, double qmax);
    public abstract void resolvePhase(int days);

    protected String getRandomName() {
        if(names == null || names.size() == 0) {
            return "";
        }

        if(names.size() == 1) {
            return names.get(0);
        }

        return names.get(random.nextInt(names.size()));
    }
}
