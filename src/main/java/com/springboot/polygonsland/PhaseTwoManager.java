package com.springboot.polygonsland;

import com.springboot.polygonsland.polygons.*;

import java.util.ArrayList;

public class PhaseTwoManager extends PhaseManager {
    private int childrenMin = 1;
    private int childrenMax = 1;

    @Override
    public void init(int n, double qmin, double qmax) {
        polygonsMerger = new PolygonsMerger(n);
        names = new ArrayList<>(10);
        names.add("PINO");
        names.add("MAURO");
        names.add("ANGELA");
        names.add("LUISA");
        names.add("ANGELO");
        names.add("ANGELA");
        names.add("MARIA");
        names.add("MARIO");
        names.add("LUIGI");
        names.add("SARA");

        for (; n > 0; n--) {
            Polygon polygon = null;

            if (random.nextInt(2) == 0) {
                polygon = new Square(qmin + (qmax - qmin) * random.nextDouble(), getRandomName());

                System.out.println("Nuovo " + polygon.getType() + " creato.\n- Lato: " + polygon.getWidth() + ".\n");
            } else {

                polygon = new Rectangle(qmin + (qmax - qmin) * random.nextDouble(), qmin + (qmax - qmin) * random.nextDouble(), getRandomName());

                System.out.println("Nuovo " + polygon.getType() + " creato.\n- Larghezza: " + polygon.getWidth() + ".\n- Altezza: " + polygon.getHeight() + ".\n");
            }

            polygonsMerger.addPolygon(polygon);
        }
    }

    public void setChildrenPerDay(int min, int max) {
        childrenMin = min;
        childrenMax = max;
    }

    @Override
    public void resolvePhase(int days) {
        int day = 1;
        int dayMerges = childrenMin + random.nextInt(childrenMax - childrenMin);

        while (day <= days && polygonsMerger.getPoligonsNumber() > 1) {
            System.out.println("Giorno " + day);

            int i = 0;

            while (i < dayMerges && polygonsMerger.getPoligonsNumber() > 1) {

                Polygon mergeResult = polygonsMerger.mergePolygons(getRandomName());

                if(!(mergeResult instanceof Triangle)) {
                    polygonsMerger.addPolygon(mergeResult);
                }

                System.out.println(mergeResult.getName() + " creato.\n- Larghezza: " + mergeResult.getWidth()
                        + ".\n- Altezza: " + mergeResult.getHeight()
                        + ".\n- Area: " + mergeResult.getArea() + ".\n"
                );

                i++;
            }

            day++;
        }
    }
}
