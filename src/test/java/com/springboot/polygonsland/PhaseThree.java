package com.springboot.polygonsland;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PhaseThree {
    @InjectMocks
    private PhaseThreeManager phaseThreeManager;

    @Test
    public void phaseOne() {
        phaseThreeManager.init(5, 1, 10);

        phaseThreeManager.setChildrenPerDay(1, 4);

        phaseThreeManager.resolvePhase(3);
    }
}
