package com.springboot.polygonsland;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PhaseOne {
    @InjectMocks
    private PhaseOneManager phaseOneManager;

    @Test
    public void phaseOne() {
        phaseOneManager.init(5, 1, 10);

        phaseOneManager.resolvePhase(3);
    }
}
