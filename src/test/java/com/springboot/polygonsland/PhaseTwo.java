package com.springboot.polygonsland;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PhaseTwo {
    @InjectMocks
    private PhaseTwoManager phaseTwoManager;

    @Test
    public void phaseOne() {
        phaseTwoManager.init(5, 1, 10);

        phaseTwoManager.setChildrenPerDay(1, 4);

        phaseTwoManager.resolvePhase(3);
    }
}
